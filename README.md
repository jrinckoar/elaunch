# Simulation Launcher Pack 
-------------------------------------------------------------------------
## Juan Felipe Restrepo. 
### jf.restrepo.rinckoar@bioingenieria.edu.ar.
-------------------------------------------------------------------------
#### Description

1.  SimConfig: setup simulation ".m" files.  
It generates all MATLAB and config files used by Elaunch and Launcher.

In the header of the matlab file, set:

```

% Simulation Config.
%   FILE NAME
%$@N Exp_48_<V>@1_<N>@2_<SNR>@3
%   VARIABLES 
%$@V1 voice_state
%$@V1 1 2 
%$@V2 data_length
%$@V2 1 2 3
%$@V3 noise_level
%$@V3 1 2 3
%   REALIZATIONS 
%$@R Exp48
%$@R 1 2 3 
%   NEPTUNO CONFIG FILE
%$& sim_path=/home/jrestrepo/bin/Elaunch/
%$& number_nodes=3
%$& email=jf.restrepo.rinckoar@bioingenieria.edu.ar

```

2.  Elaunch: setup the files to be ran over Neptuno.

3.  Launcher: launch the matlab processes in the background.

4.  PCheck: Check if there are processes running in Neptuno's nodes.

-------------------------------------------------------------------------

### Usage 

1. SimConfig <Simulation-File.m>
2. Elaunch  <Simulation-ConfigFile.cfg>
3. Launcher <Simulation-ConfigFile.cfg>
4. PCheck

-------------------------------------------------------------------------


