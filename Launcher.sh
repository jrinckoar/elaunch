#!/bin/bash
# Launcher
LAUNCHERPID=$$
VERSION='3.0'
PNAME='Launcher'
#========================================================================================
#========================================================================================
#----------------------------------------------------------------------------------------
# PARCE FLAGS AND HELP 
HELP=0
IINDEX=1
FINDEX=0
LOGFILE="Simulation.log"
CHECHMARK='@@@ JOB DONE @@@'
for i in "$@"; do
    case $i in
    -nstart=*)
        IINDEX=$(echo "${i#*=}" | sed -e 's/[^0-9]//g')
    ;;
    -nstop=*)
        FINDEX=$(echo "${i#*=}" | sed -e 's/[^0-9]//g')
    ;;
    -nlist=*)
        LIST=($(echo "${i#*=}" | sed -e 's/.*\[\([^]]*\)\].*/\1/g' | sed -e 's/,/ /g'))
    ;;
    *-h | *-help)
        HELP=1
    ;;        
    *)
    ;;
    esac
done

if [[ "$HELP" -eq 1 ]]; then
usage="$PNAME [-h -nstart= -nstop=] input-file -- program to launch the simulation files

where:
    -nstart= start from simulation nstart
    -nstop= stop after simulation nstop
    -h   show this help text"
    echo "usage: $usage"
    exit
fi

#----------------------------------------------------------------------------------------
# SUPPORT FUNCTIONS 
function clean_up() {
	# Perform program exit housekeeping
	# Optionally accepts an exit status
	rm -f "${TMPFILE}"
    if [[ $1 -eq 0 ]];then
        echo "--------------------------------------------------------------" >> $LOGFILE
        echo 'SIMULATON FINISHED' >> ${LOGFILE}
        exit $1
    else
	    exit $1
    fi
}
function error_exit() {
	# Display error message and exit
	echo "${PNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "Try ${PNAME} -h for help"
	clean_up 1
}
function Email(){
    process=$2
    mail=$3
    Host=$(hostname)
    if [[ "$1" -eq 1 ]];then
        Status="error"
    else
        Status="finished"
    fi
    subject="${Host}'s Simulation ${Status}"
    echo " Simulation ${process} ${Status} on host ${Host} at `date +"%m-%d-%y %T"`." | mailx -s "${subject}" "${mail}"
}
#----------------------------------------------------------------------------------------
# CHECK INPUT FILE
MAINPATH=`pwd`

INPUT=$(echo $@ | sed -e  "s/-nstart=[a-zA-Z]*[0-9]*//g")
INPUT=$(echo $INPUT | sed -e  "s/-nstop=[a-zA-Z]*[0-9]*//g")
INPUT=$(echo $INPUT | sed -e  's/-nlist.*\]//g')
INPUT=$(echo $INPUT | sed -e 's/^ *//' -e 's/ *$//')

if [[ ! -f "$INPUT" ]]; then
    error_exit "No input file provided or it does not exist."
else
    if [[ -s "$INPUT" ]]; then
        MAINFILE=$INPUT
    else
        error_exit "Input file is empty"
    fi
fi


#----------------------------------------------------------------------------------------
# READING INPUT FILE
source "$MAINPATH/$MAINFILE"
#----------------------------------------------------------------------------------------
TMPFILE=$(mktemp "${MAINPATH}/tmp.XXXXX")

# CREATE LOG FILE
if [[ -f "${LOGFILE}" ]] ; then
    temp=`date +"%m-%d-%y   %T"`
    echo "--------------------------------------------------------------" >> $LOGFILE
    echo " Relaunch | PID=${LAUNCHERPID} | ${temp} " >> $LOGFILE
    echo "--------------------------------------------------------------" >> $LOGFILE
    echo -e '\tFILE \t\t   START TIME\t   PID\t  ELAPSED TIME' >> ${LOGFILE}
else
    touch $LOGFILE
    echo "--------------------------------------------------------------" >> $LOGFILE
    echo "$PNAME Version $VERSION" >> ${LOGFILE}
    echo "Log File" >> $LOGFILE
    echo "--------------------------------------------------------------" >> $LOGFILE
    temp=`date +"%m-%d-%y   %T"`
    echo " Simulation ${sim_name} | PID=${LAUNCHERPID} | ${temp} " >> $LOGFILE
    echo "--------------------------------------------------------------" >> $LOGFILE
    echo -e '\tFILE \t\t   START TIME\t   PID\t  ELAPSED TIME' >> ${LOGFILE}
fi
#----------------------------------------------------------------------------------------
# LAUNCH MATLAB PROCESSES
CSUM=$((${IINDEX}-1))

if [[ "$FINDEX" -eq 0 ]]; then
   FINDEX=${sim_number}
else
    if [[ "$FINDEX" -gt ${sim_number} ]]; then
        error_exit "nstop (${FINDEX}) > total number of simulations (${sim_number})"
    fi
fi 

if [[ "${FINDEX}" -gt "${sim_number}" ]]; then
    error_exit "nstart (${IINDEX}) > total number of simulations (${sim_number})"
fi

if [[ "${FINDEX}" -lt "${IINDEX}" ]]; then
    error_exit "nstart (${IINDEX}) > nstop (${FINDEX})" 
fi

SIMLIST=${LIST[@]}
if [[ "${#LIST[@]}" -eq 0 ]]; then
    SIMLIST=($(eval "echo {${IINDEX}..${FINDEX}}"))
fi

#for ((k=${IINDEX};k<=${FINDEX};k++)); do
for k in ${SIMLIST[@]}; do
    
    cd $sim_path
    if [[ "$k" -lt 10 ]]; then
        PRONAME="${sim_name}_0${k}.m"
        PCHECKFILE="${job_outfile_prefix}-0${k}${job_outfile_extension}"
        PROCCESS="${job_matlab} ${job_matlab_flags} < ${sim_name}_0${k}.m > ${job_outfile_prefix}-0${k}${job_outfile_extension} 2> ${job_errorfile_prefix}-0${k}${job_errorfile_extension}"
    else
        PRONAME="${sim_name}_${k}.m"
        PCHECKFILE="${job_outfile_prefix}-${k}${job_outfile_extension}"
        PROCCESS="${job_matlab} ${job_matlab_flags} < ${sim_name}_${k}.m > ${job_outfile_prefix}-${k}${job_outfile_extension} 2> ${job_errorfile_prefix}-${k}${job_errorfile_extension}"
    fi

    /usr/bin/time -f '%E' -o ${TMPFILE} ${PROCCESS} &
    PROCESSESPID=$!
    PROCESSESPID=$(ps -Af | grep "$PROCESSESPID" | grep -v "time" |grep "matlab" | awk '{print $2}')
    temp=`date +"%d %T"`
    echo -e "${PRONAME}\t  $temp\t  ${PROCESSESPID}" >> ${LOGFILE}
    while [ -e "/proc/${PROCESSESPID}" ]; do 
        sleep 10
    done

    if [[ ! -f "$PCHECKFILE" ]]; then
        error_exit "No output file was generated."
    else
        if  grep -q "${CHECHMARK}" "${PCHECKFILE}" ; then 
            echo -e "$(cat ${LOGFILE})\t     $(cat ${TMPFILE})" > ${LOGFILE}
            let "CSUM+=1" 
        else
            Email 1 "${PRONAME}" "${job_email}"
            temp=`date +"%d %T"`
            echo -e "$(cat ${LOGFILE})\t  Error $temp" > ${LOGFILE}
            if ps aux | grep "[M]ATLAB" > /dev/null ; then
                kill $PROCESSPID
            fi
            clean_up 1
        fi
    fi
done

if [[ "${CSUM}" -eq "${sim_number}" ]]; then
    Email 0 "${sim_name}" "${job_email}"
    clean_up 0 
else
    error_exit "Simulations are incomplete."
fi
cd $MAINPATH
