#!/bin/bash
# SimConfig

VERSION='1.0'
PNAME='PCheck'
#========================================================================================
#========================================================================================
#----------------------------------------------------------------------------------------
# PARCE FLAGS AND HELP 
NODES=($(echo {1..50}))
PROCESS='MATLAB'
USER='jrestrepo'
HELP=0
for i in "$@"; do
    case $i in
    -n=*)
        NODES=($(echo $@ | sed -e 's/-n=//' -e "s/$PNAME//" -e 's/^ *//' -e 's/ *$//'))
        echo ${NODES[0]}
        echo ${NODES[1]}
        echo ${NODES[2]}
    ;;        
    *-h | *-help)
        HELP=1
    ;;        
    *)
    ;;
    esac
done

if [[ "$HELP" -eq 1 ]]; then
usage="$PNAME [-h] [-n]=node_number/node_list -- program to check if there is any MATLAB 
                                                 process running on neptuno. 

where:
    -h                          show this help text.
    -n=node_number/node_list    Check in a specific node or node list i.e n=1 2 3 4."
    echo "usage: $usage"
    exit
fi
#----------------------------------------------------------------------------------------
# SUPPORT FUNCTIONS 
function clean_up() {
	# Perform program exit housekeeping
	# Optionally accepts an exit status
	rm -f "${TMPFILE}"
	exit $1
}
function error_exit() {
	# Display error message and exit
	echo "${PNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "Try ${PNAME} -h for help"
	clean_up 1
}
#----------------------------------------------------------------------------------------
# CREATE TEMPORAL FILE
MAINOPATH=`pwd` 
TMPFILE=$(mktemp "${MAINOPATH}/tmp.XXXXX")

POS_UID=0
POS_PID=1
POS_CMD=7

# CHECK FOR USER PROCESS IN NODES
count=0
echo -e 'NODE\tUSER  \t\tPID  \tCMD' >> ${TMPFILE}
echo '-------------------------------------------------------------------------' >> ${TMPFILE}
for i in "${NODES[@]}"; do
    echo "Checking node ${i}"
    #temp=($(ps -fu ${USER} | grep "${PROCESS}" | sed '/grep/d')) > /dev/null
    temp=($(ssh -n nodo"${i}" "ps -fu ${USER}" | grep "${PROCESS}" | sed '/grep/d')) > /dev/null
    if [[ ! -z "${temp[@]}" ]]; then
        let "count++"
        uname=$(awk -v val="${temp[$POS_UID]}" -F ":" '$3==val{print $1}' /etc/passwd)
        echo -e "$i\t${uname}\t${temp[$POS_PID]}\t${temp[$POS_CMD]}" >> ${TMPFILE}
    fi
done

# SHOW OUTPUT
if [[ "${count}" -ne 0 ]];then
    cat ${TMPFILE}
else
    echo "NO PROCESSES FOUND"
fi
# REMOVE TMP FILE
clean_up
