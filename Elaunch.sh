#!/bin/bash
# Elaunch
VERSION='1.2'
PNAME='Elaunch'
#========================================================================================
#========================================================================================
#----------------------------------------------------------------------------------------
# PARCE FLAGS AND HELP 
EXECUTE=0
HELP=0
for i in "$@"; do
    case $i in
    -ex)
        EXECUTE=1
    ;;
    -nu)

    ;;
    *-h | *-help)
        HELP=1
    ;;        
    *)
    ;;
    esac
done

if [[ "$HELP" -eq 1 ]]; then
usage="$PNAME [-h] [-ex] input-file -- program to set up simulation files

where:
    -h  show this help text
    -ex Execute processes
    -nc Do not create configuration files"
    echo "usage: $usage"
    exit
fi
#----------------------------------------------------------------------------------------
# SUPPORT FUNCTIONS 
function clean_up() {
	# Perform program exit housekeeping
	# Optionally accepts an exit status
	#rm -f "${TMPFILE}"
	#rm -f "${CTMPFILE}"
	exit $1
}
function error_exit() {
	# Display error message and exit
	echo "${PNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "Try ${PNAME} -h for help"
	clean_up 1
}
#----------------------------------------------------------------------------------------
# CHECK INPUT FILE
MAINPATH=`pwd`

INPUT=$(echo $@ | sed -e  "s/-ex//g")
INPUT=$(echo $INPUT | sed -e 's/^ *//' -e 's/ *$//')

if [[ ! -f "$INPUT" ]]; then
    error_exit "No input file provided or it does not exist."
else
    if [[ -s "$INPUT" ]]; then
        MAINFILE=$INPUT
    else
        error_exit "Input file is empty"
    fi
fi
#----------------------------------------------------------------------------------------
# READING INPUT FILE
echo "$PNAME Version $VERSION"
source "$MAINPATH/$MAINFILE"
#----------------------------------------------------------------------------------------
# CHECK VARIABLES

# Mandatory
vars=('sim_name' 'sim_path' 'sim_number') 
check=0;
for i in ${vars[*]}; do
    input=$(eval "echo \$$i") 
    if [ -z "$input" -a "${input+x}" = "x" ]; then
        missing_vars[$check]=${vars[$i]}  
        let "check++"
    fi
done
if [ $check -gt 0 ]; then
    for i in ${missing_vars[*]}; do
        echo $i
    done
    error_exit "Requared variables are not set"
fi

# No mandatory
if ([[ "$EXECUTE" == 1 ]]);then
vars=('node_option' 'number_nodes' 'job_id' 'job_email' 'job_matlab' 'job_matlab_flags' 'job_outfile_prefix' 'job_outfile_extension' 'job_errorfile_prefix' 'job_errorfile_extension')
vals=('mn' '4' 'sim_name' 'user@email.com' 'matlab' '-nodisplay -nojvm -nosplash' 'out' '.txt' 'eout' '.er')
for i in ${!vars[*]}; do
    variable=$(eval "echo \${vars[$i]}")
    value=$(eval "echo \${vals[$i]}")
    input=$(eval "echo \$${vars[$i]}")
    if [ -z "$input" -a "${input+x}" = "x" ]; then
        declare "${variable}"="${value}"
    fi
done
fi
#----------------------------------------------------------------------------------------
# SET TEXT FILES
echo "Setting files...."

let nfiles=($sim_number / $number_nodes)
let rpros=($sim_number % $number_nodes)

n_Tcount=0;
n_Icount=1
n_Fcount=$number_nodes
serie=($(eval "echo {a..z}"))

for k in $(eval echo {0..$(($nfiles-1))}); do
    fnames[$k]="$sim_name-${serie[$k]}.txt"
    if [ -f ${fnames[$k]} ]; then
        eval "rm ${fnames[$k]}"
        echo "$sim_path" > ${fnames[$k]}
    else
        echo "$sim_path" > ${fnames[$k]}
    fi
    for i in $(eval echo {$n_Icount..$n_Fcount});do
        if [[ "$i" -lt 10 ]]; then
            echo "${job_matlab} ${job_matlab_flags} < ${sim_name}_0${i}.m > ${job_outfile_prefix}-0${i}${job_outfile_extension} 2> ${job_errorfile_prefix}-0${i}${job_errorfile_extension}" >> ${fnames[$k]}
            let "n_Tcount+=1"
        else
            echo "${job_matlab} ${job_matlab_flags} < ${sim_name}_${i}.m > ${job_outfile_prefix}-${i}${job_outfile_extension} 2> ${job_errorfile_prefix}-${i}${job_errorfile_extension}" >> ${fnames[$k]}
            let "n_Tcount+=1"
        fi
    done
    
    n_Icount=$(($n_Icount+$number_nodes))
    n_Fcount=$(($n_Fcount+$number_nodes))
done
if [ $rpros -gt 0 ]; then
    nfiles=$(($nfiles+1))
    k=$(($k+1))
    n_Fcount=$(($n_Fcount-$number_nodes+$rpros))
    fnames[$k]="$sim_name-${serie[$k]}.txt"
    if [ -f ${fnames[$k]} ]; then
        eval "rm ${fnames[$k]}"
        echo "$sim_path" > ${fnames[$k]}
    else
        echo "$sim_path" > ${fnames[$k]}
    fi
    for i in $(eval echo {$n_Icount..$n_Fcount});do
        if [[ "$i" -lt 10 ]]; then
            echo "${job_matlab} ${job_matlab_flags} < ${sim_name}_0${i}.m > ${job_outfile_prefix}-0${i}${job_outfile_extension} 2> ${job_errorfile_prefix}-0${i}${job_errorfile_extension}" >> ${fnames[$k]}
            let "n_Tcount+=1"
        else
            echo "${job_matlab} ${job_matlab_flags} < ${sim_name}_${i}.m > ${job_outfile_prefix}-${i}${job_outfile_extension} 2> ${job_errorfile_prefix}-${i}${job_errorfile_extension}" >> ${fnames[$k]}
            let "n_Tcount+=1"
        fi
    done
fi

cfiles=0;
if [ $n_Tcount -eq $sim_number ]; then
    for k in $(eval echo {0..$(($nfiles-1))}); do
        if [ -f ${fnames[$k]} ]; then
            let "cfiles+=1"
        fi
    done
    
    if [ $cfiles -eq $nfiles ]; then
        echo "Files OK" >&2
    else
        for k in $(eval echo {0..$(($nfiles-1))}); do
            if [ -f ${fnames[$k]} ]; then
                eval "rm ${fnames[$k]}"
            fi
        done
        error_exit "Files can not be created...."
    fi
fi

# Execute processes
#----------------------------------------------------------------------
EXEC_KEY=0
if ([[ "$EXECUTE" == 1 ]]);then
    echo "Do you want to launch the processes ?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) EXEC_KEY=1; break;;
            No ) exit;;
        esac
    done
    if [[ "${EXEC_KEY}" -eq 1 ]]; then
        echo "Launching processes"
        for k in $(eval echo {0..$(($nfiles-1))}); do
            pro=''
            if [ -f ${fnames[$k]} ]; then
                nlines=$(wc -l < "${fnames[$k]}")
                nlines=$(($nlines-1))
                encola -${node_option} ${nlines} -d ${job_id}-${serie[$k]} -mail ${job_email} ${sim_path}/${fnames[$k]}
                sleep 5s
            fi
        done
    fi
fi
    












    #echo "Executing files..." >&2
    #for k in $(eval echo {0..$(($nfiles-1))}); do
        #if [ -f ${fnames[$k]} ]; then
            #nlines=$(wc -l < "${fnames[$k]}")
            #nlines=$(($nlines-1))
            #pro=$(eval "encola -${job_nodeop} ${nlines} -d ${job_id}-${serie[$k]} -mail ${email} ${sim_path}/${fnames[$k]}")
            #echo "encola -${job_nodeop} ${nlines} -d ${job_id}-${serie[$k]} -mail ${email} ${sim_path}/${fnames[$k]}"
            #pro=$(eval "cat ${fnames[$k]}")
            #if [ -z "${pro}" ];then
                #echo $pro
                #echo "Job can not be Sent" >&2
                #exit 0
            #else
                #echo "Sent" >&2
            #fi
            #sleep 12s
        #fi
    #done
#fi
#echo "Done..." >&2









